<?php

/**
 * project-bk - a PHP Framework for rapid developing
 *
 * @package  project-bk
 * @author   Baris Kalaycioglu <thecodemasterzz@gmail.com>
 */

define("BASE_URL", 		'http://project-bk/' );
define("THEME_NAME", 	'default' );