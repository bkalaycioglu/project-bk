<?php

/**
 * project-bk - a PHP Framework for rapid developing
 *
 * @package  project-bk
 * @author   Baris Kalaycioglu <thecodemasterzz@gmail.com>
 */

$klein = new \Klein\Klein();


$klein->respond('/api.[xml|csv|json:format]?', function ($request, $response, $service) {
    $send = $request->param('format', 'json');
    if ( $send === "json" ) {
		Header('Content-type: text/json');
    	return json_encode( array("value" => "Hello world!" )  );
    } else if ( $send === "xml" ) {
    	$xml = new SimpleXMLElement('<xml/>');
    	$value = $xml->addChild("value", "Hello world!");
		Header('Content-type: text/xml');
		return $xml->asXML();
    } else if ( $send === "csv" ) {
		$out = fopen('php://output', 'w');
		fputcsv($out, array("value" => "Hello world!" ) );
		die;
    }
});

$klein->dispatch();

die;
