<?php

namespace DatabaseName;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrtakIl
 *
 * @ORM\Table(name="ortak_il")
 * @ORM\Entity
 */
class OrtakIl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="il_adi", type="string", nullable=false)
     */
    private $ilAdi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eklenme_tarihi", type="datetime", nullable=false)
     */
    private $eklenmeTarihi;

    /**
     * @var integer
     *
     * @ORM\Column(name="uyelik_kimlik_id_ekleyen", type="integer", nullable=false)
     */
    private $uyelikKimlikIdEkleyen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="son_guncelleme_tarihi", type="datetime", nullable=true)
     */
    private $sonGuncellemeTarihi;

    /**
     * @var integer
     *
     * @ORM\Column(name="uyelik_kimlik_id_guncelleyen", type="integer", nullable=true)
     */
    private $uyelikKimlikIdGuncelleyen;

    /**
     * @var string
     *
     * @ORM\Column(name="silindi_mi", type="string", length=5, nullable=true)
     */
    private $silindiMi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="silinme_tarihi", type="datetime", nullable=true)
     */
    private $silinmeTarihi;

    /**
     * @var integer
     *
     * @ORM\Column(name="uyelik_kimlik_id_silen", type="integer", nullable=true)
     */
    private $uyelikKimlikIdSilen;

    /**
     * @var integer
     *
     * @ORM\Column(name="nesne_versiyon_numarasi", type="integer", nullable=true)
     */
    private $nesneVersiyonNumarasi;

    /**
     * @var string
     *
     * @ORM\Column(name="nesne_versiyon_tarihcesi", type="text", nullable=true)
     */
    private $nesneVersiyonTarihcesi;

    /**
     * @var integer
     *
     * @ORM\Column(name="uyelik_id_basvuran", type="integer", nullable=true)
     */
    private $uyelikIdBasvuran;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ilAdi
     *
     * @param string $ilAdi
     *
     * @return OrtakIl
     */
    public function setIlAdi($ilAdi)
    {
        $this->ilAdi = $ilAdi;
    
        return $this;
    }

    /**
     * Get ilAdi
     *
     * @return string
     */
    public function getIlAdi()
    {
        return $this->ilAdi;
    }

    /**
     * Set eklenmeTarihi
     *
     * @param \DateTime $eklenmeTarihi
     *
     * @return OrtakIl
     */
    public function setEklenmeTarihi($eklenmeTarihi)
    {
        $this->eklenmeTarihi = $eklenmeTarihi;
    
        return $this;
    }

    /**
     * Get eklenmeTarihi
     *
     * @return \DateTime
     */
    public function getEklenmeTarihi()
    {
        return $this->eklenmeTarihi;
    }

    /**
     * Set uyelikKimlikIdEkleyen
     *
     * @param integer $uyelikKimlikIdEkleyen
     *
     * @return OrtakIl
     */
    public function setUyelikKimlikIdEkleyen($uyelikKimlikIdEkleyen)
    {
        $this->uyelikKimlikIdEkleyen = $uyelikKimlikIdEkleyen;
    
        return $this;
    }

    /**
     * Get uyelikKimlikIdEkleyen
     *
     * @return integer
     */
    public function getUyelikKimlikIdEkleyen()
    {
        return $this->uyelikKimlikIdEkleyen;
    }

    /**
     * Set sonGuncellemeTarihi
     *
     * @param \DateTime $sonGuncellemeTarihi
     *
     * @return OrtakIl
     */
    public function setSonGuncellemeTarihi($sonGuncellemeTarihi)
    {
        $this->sonGuncellemeTarihi = $sonGuncellemeTarihi;
    
        return $this;
    }

    /**
     * Get sonGuncellemeTarihi
     *
     * @return \DateTime
     */
    public function getSonGuncellemeTarihi()
    {
        return $this->sonGuncellemeTarihi;
    }

    /**
     * Set uyelikKimlikIdGuncelleyen
     *
     * @param integer $uyelikKimlikIdGuncelleyen
     *
     * @return OrtakIl
     */
    public function setUyelikKimlikIdGuncelleyen($uyelikKimlikIdGuncelleyen)
    {
        $this->uyelikKimlikIdGuncelleyen = $uyelikKimlikIdGuncelleyen;
    
        return $this;
    }

    /**
     * Get uyelikKimlikIdGuncelleyen
     *
     * @return integer
     */
    public function getUyelikKimlikIdGuncelleyen()
    {
        return $this->uyelikKimlikIdGuncelleyen;
    }

    /**
     * Set silindiMi
     *
     * @param string $silindiMi
     *
     * @return OrtakIl
     */
    public function setSilindiMi($silindiMi)
    {
        $this->silindiMi = $silindiMi;
    
        return $this;
    }

    /**
     * Get silindiMi
     *
     * @return string
     */
    public function getSilindiMi()
    {
        return $this->silindiMi;
    }

    /**
     * Set silinmeTarihi
     *
     * @param \DateTime $silinmeTarihi
     *
     * @return OrtakIl
     */
    public function setSilinmeTarihi($silinmeTarihi)
    {
        $this->silinmeTarihi = $silinmeTarihi;
    
        return $this;
    }

    /**
     * Get silinmeTarihi
     *
     * @return \DateTime
     */
    public function getSilinmeTarihi()
    {
        return $this->silinmeTarihi;
    }

    /**
     * Set uyelikKimlikIdSilen
     *
     * @param integer $uyelikKimlikIdSilen
     *
     * @return OrtakIl
     */
    public function setUyelikKimlikIdSilen($uyelikKimlikIdSilen)
    {
        $this->uyelikKimlikIdSilen = $uyelikKimlikIdSilen;
    
        return $this;
    }

    /**
     * Get uyelikKimlikIdSilen
     *
     * @return integer
     */
    public function getUyelikKimlikIdSilen()
    {
        return $this->uyelikKimlikIdSilen;
    }

    /**
     * Set nesneVersiyonNumarasi
     *
     * @param integer $nesneVersiyonNumarasi
     *
     * @return OrtakIl
     */
    public function setNesneVersiyonNumarasi($nesneVersiyonNumarasi)
    {
        $this->nesneVersiyonNumarasi = $nesneVersiyonNumarasi;
    
        return $this;
    }

    /**
     * Get nesneVersiyonNumarasi
     *
     * @return integer
     */
    public function getNesneVersiyonNumarasi()
    {
        return $this->nesneVersiyonNumarasi;
    }

    /**
     * Set nesneVersiyonTarihcesi
     *
     * @param string $nesneVersiyonTarihcesi
     *
     * @return OrtakIl
     */
    public function setNesneVersiyonTarihcesi($nesneVersiyonTarihcesi)
    {
        $this->nesneVersiyonTarihcesi = $nesneVersiyonTarihcesi;
    
        return $this;
    }

    /**
     * Get nesneVersiyonTarihcesi
     *
     * @return string
     */
    public function getNesneVersiyonTarihcesi()
    {
        return $this->nesneVersiyonTarihcesi;
    }

    /**
     * Set uyelikIdBasvuran
     *
     * @param integer $uyelikIdBasvuran
     *
     * @return OrtakIl
     */
    public function setUyelikIdBasvuran($uyelikIdBasvuran)
    {
        $this->uyelikIdBasvuran = $uyelikIdBasvuran;
    
        return $this;
    }

    /**
     * Get uyelikIdBasvuran
     *
     * @return integer
     */
    public function getUyelikIdBasvuran()
    {
        return $this->uyelikIdBasvuran;
    }
}

